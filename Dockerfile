FROM openjdk:8-jdk
ARG AMPS_PLUGIN_VERSION

RUN apt-get update && apt-get upgrade && \
	apt-get install -y make build-essential python3

RUN curl -Lo atlassian-sdk.tgz https://marketplace.atlassian.com/download/plugins/atlassian-plugin-sdk-tgz && \
	mkdir -p sdk && \
	tar -xf atlassian-sdk.tgz -C sdk --strip 1 && \
	rm -f atlassian-sdk.tgz
ENV PATH="/sdk/bin:${PATH}"

WORKDIR /wspace/filedownloader
COPY src src
COPY pom.xml pom.xml

CMD [atlas-version]
