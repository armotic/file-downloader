[#import "/lib/ace.ftl" as ace ]
[@ww.checkbox id="fd_iscreation" name='iscreation'/]

[@ww.select id="fd_filetype" labelKey="f.a.b.p.f.label.filetype" required=true toggle=true
listKey="key"
listValue="value"
list="filetypes"
name="filetype" /]

<fieldset class="dependsOnfiletype showOnTEXT showOnSCRIPT">
[@ww.select id="fd_variablepattern" labelKey="f.a.b.p.f.label.variablepattern" required=true
listKey="key"
listValue="value"
list="variablepatterns"
name="variablepattern" /]
</fieldset>

[@ui.bambooSection dependsOn="filetype" showOn="ARCHIVE"]
    [@ww.checkbox id="fd_archiveextraction" labelKey="f.a.b.p.f.label.archiveextraction" name="archiveextraction" /]
[/@ui.bambooSection]

<hr/>

[@ww.select id="fd_locationtype" labelKey="f.a.b.p.f.label.locationtype" required=true toggle=true
listKey="key"
listValue="value"
list="locationtypes"
name="locationtype" /]

[@ui.bambooSection dependsOn="locationtype" showOn="INLINE"]
    [@ww.component id="fd_inlinebody" template="ace-textarea.ftl" labelKey="f.a.b.p.f.label.inlinebody" required=true
    name="inlinebody" /]
    [@ww.select id="fd_lineending" labelKey="f.a.b.p.f.label.lineendings" required=true
    listKey="key"
    listValue="value"
    list="lineendings"
    name="lineending"/]
[/@ui.bambooSection]

[@ui.bambooSection dependsOn="locationtype" showOn="ONLINE"]
    [@ww.textfield id="fd_onlinelocation" labelKey="f.a.b.p.f.label.onlinelocation" name="onlinelocation" cssClass="long-field" required='true'/]
    [@ww.select id="fd_authenticationtype" labelKey="f.a.b.p.f.label.authenticationtype" required=true toggle=true
    listKey="key"
    listValue="value"
    list="authenticationtypes"
    name="authenticationtype" /]
    [@ui.bambooSection dependsOn="authenticationtype" showOn="BASIC"]
        [@ww.textfield id="fd_login" labelKey='f.a.b.p.f.label.login' name='login' required=true /]
        [@ww.password id="fd_password" labelKey='f.a.b.p.f.label.password' name='password' required=true /]
    [/@ui.bambooSection]
    [@s.checkbox id="fd_onlinetest" labelKey='f.a.b.p.f.label.onlinelocation.test' name='onlinetest' /]
[/@ui.bambooSection]

[@ui.bambooSection dependsOn="locationtype" showOn="REPOSITORY"]
    [@ww.select id="fd_repository" labelKey="f.a.b.p.f.label.repository" required=true toggle=true
    listKey="key"
    listValue="value"
    list="repositories"
    name="repository" /]
    [@ww.textfield id="fd_fileorigin" labelKey="f.a.b.p.f.label.fileorigin" name="fileorigin" cssClass="long-field" required='true'/]
[/@ui.bambooSection]

<hr/>

[@ww.textfield id="fd_filedestination" labelKey='f.a.b.p.f.label.filedestination' name='filedestination' cssClass="long-field" required=true /]

<script type="application/javascript">
    jQuery(function ($) {
        console.log("Inside");
        var iscreation = $("#fd_iscreation");

        var filetype = $("#fd_filetype");
        var variablepattern = $("#fd_variablepattern");
        var archiveextraction = $("#fd_archiveextraction");

        var locationtype = $("#fd_locationtype");
        var inlinebody = $("#fd_inlinebody");
        var lineending = $("#fd_lineending");
        var onlinelocation = $("#fd_onlinelocation");
        var onlinetest = $("#fd_onlinetest");
        var authenticationtype = $("#fd_authenticationtype");
        var login = $("#fd_login");
        var password = $("#fd_password");
        var repokey = $("#fd_repokey");
        var repobranch = $("#fd_repobranch");
        var fileorigin = $("#fd_fileorigin");

        var filedestination = $("#fd_filedestination");

        iscreation.parent().parent().css('display', 'none');
        if (authenticationtype.val() == 'BASIC'
                && !password.val()) {
            onlinetest.parent().hide()
        } else {
            onlinetest.parent().show();
        }

        repobranch.attr('placeholder', "default");

        if (!iscreation.is(':checked') && authenticationtype.val() == 'BASIC') {
            password.attr('placeholder', "Your current password is hidden");
        }

        filetype.on('change', function() {
            if (filetype.val() == 'ARCHIVE') {
                variablepattern.val('NONE').trigger('change');
                if (locationtype.find('option:selected').val() == 'INLINE') {
                    locationtype.val('ONLINE').trigger('change');
                }
                locationtype.find('option[value="INLINE"]')
                        .attr("disabled", "disabled");
            } else {
                locationtype.find('option[value="INLINE"]')
                        .removeAttr("disabled");
            }
        });
        authenticationtype.on('change', function() {
            if (authenticationtype.val() == 'BASIC'
                    && !password.val()) {
                onlinetest.parent().hide(200)
            } else {
                onlinetest.parent().show(200);
            }
        });
        password.on('input', function() {
            if (!password.val()) {
                onlinetest.parent().hide(200)
            } else {
                onlinetest.parent().show(200);
            }
        });

    });
</script>