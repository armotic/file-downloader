/*
 * Copyright 2016 Armotic
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.armotic.bamboo.plugins.filedownloader.core;

import com.atlassian.core.util.PairType;

import java.util.regex.Pattern;

public class VariablePattern extends PairType {
    public final String id;
    private String whatToShow;
    private Pattern pattern;
    private boolean doTrim;

    public VariablePattern(
            String id,
            String whatToShow,
            String pattern,
            boolean doTrim
    ) {
        super(id, whatToShow);
        this.id = id;
        this.whatToShow = whatToShow;
        this.pattern = Pattern.compile(pattern);
        this.doTrim = doTrim;
    }

    public Pattern getPattern() {
        return this.pattern;
    }

    public boolean hasToBeTrimmed() {
        return this.doTrim;
    }

    public String toString() {
        return this.whatToShow;
    }
}
