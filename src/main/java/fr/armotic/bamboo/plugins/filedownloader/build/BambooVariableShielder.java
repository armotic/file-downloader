/*
 * Copyright 2016 Armotic
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.armotic.bamboo.plugins.filedownloader.build;

import com.google.common.base.Strings;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BambooVariableShielder {

    private static Pattern bambooVariable = Pattern.compile("\\$\\{([\\w.-]+)\\}");
    private static Pattern shieldedBambooVariable = Pattern.compile("<fd-var:([\\w.-]+)>");

    public static String shieldBambooVariables(String body) {
        if (Strings.isNullOrEmpty(body)) {
            return body;
        }
        Matcher matcher = bambooVariable.matcher(body);
        while(matcher.find()) {
            body = body.replace(matcher.group(0), "<fd-var:" + matcher.group(1) + ">");
        }
        return body;
    }

    public static String unshieldBambooVariables(String body) {
        if (Strings.isNullOrEmpty(body)) {
            return body;
        }
        Matcher matcher = shieldedBambooVariable.matcher(body);
        while(matcher.find()) {
            body = body.replace(matcher.group(0), "${" + matcher.group(1) + "}");
        }
        return body;
    }
}
