/*
 * Copyright 2016 Armotic
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.armotic.bamboo.plugins.filedownloader;

import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.core.util.PairType;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import fr.armotic.bamboo.plugins.filedownloader.core.VariablePattern;

import java.util.List;
import java.util.Map;

import static fr.armotic.bamboo.plugins.filedownloader.core.EolType.*;
import static fr.armotic.bamboo.plugins.filedownloader.core.FileType.*;

public class FileDownloaderManager {

    public static final String IS_CREATION = "iscreation";

    public static final String FILE_TYPE = "filetype";
    public static final String FILE_TYPES = "filetypes";
    public static final String VARIABLE_PATTERN = "variablepattern";
    public static final String VARIABLE_PATTERNS = "variablepatterns";
    public static final String ARCHIVE_EXTRACTION = "archiveextraction";

    public static final String LOCATION_TYPE = "locationtype";
    public static final String LOCATION_TYPES = "locationtypes";
    public static final String INLINE_BODY = "inlinebody";
    public static final String LINE_ENDING = "lineending";
    public static final String LINE_ENDINGS = "lineendings";
    public static final String ONLINE_LOCATION = "onlinelocation";
    public static final String ONLINE_TEST = "onlinetest";
    public static final String AUTHENTICATION = "authenticationtype";
    public static final String AUTHENTICATIONS = "authenticationtypes";
    public static final String LOGIN = "login";
    public static final String PASSWORD = "password";
    public static final String REPOSITORY = "repository";
    public static final String REPOSITORIES = "repositories";
    public static final String FILE_ORIGIN = "fileorigin";

    public static final String FILE_DESTINATION = "filedestination";


    public static final List<PairType> FILE_TYPE_CHOICES;
    public static final List<VariablePattern> VARIABLE_PATTERN_CHOICES;
    public static final List<PairType> LOCATION_TYPE_CHOICES;
    public static final List<PairType> LOCATION_TYPE_CHOICES_WITHOUT_REPOSITORY;
    public static final List<PairType> AUTHENTICATION_CHOICES;
    public static final List<PairType> LINE_ENDING_CHOICES;
    static {
        FILE_TYPE_CHOICES = ImmutableList.of(
                new PairType(TEXT, "Text file"),
                new PairType(SCRIPT, "Script file"),
                new PairType(ARCHIVE, "Archive"));

        LOCATION_TYPE_CHOICES = ImmutableList.of(
                new PairType("INLINE", "Inline"),
                new PairType("ONLINE", "Online"),
                new PairType("REPOSITORY", "Repository"));

        LOCATION_TYPE_CHOICES_WITHOUT_REPOSITORY = ImmutableList.of(
                LOCATION_TYPE_CHOICES.get(0),
                LOCATION_TYPE_CHOICES.get(1));

        AUTHENTICATION_CHOICES = ImmutableList.of(
                new PairType("NONE", "None"),
                new PairType("BASIC", "Basic authentication"));

        LINE_ENDING_CHOICES = ImmutableList.of(
                new PairType(LF, "LF"),
                new PairType(CRLF, "CRLF"),
                new PairType(CR, "CR"));

        VARIABLE_PATTERN_CHOICES = ImmutableList.of(
                new VariablePattern("NONE", "None", "", false),
                new VariablePattern("${VAR}"
                        , "${variable} or ${variable:-defaultValue}"
                        , "\\$\\{\\s*([\\w_.]+)\\s*(?:\\:\\-([^}]+))?\\s*\\}"
                        , true),
                new VariablePattern("$(VAR)"
                        , "$(variable) or $(variable:defaultValue)"
                        , "\\$\\(\\s*([\\w_.]+)\\s*(?:\\:\\s*([^)]+)\\s*)?\\)"
                        , true),
                new VariablePattern("@(VAR)"
                        , "@(variable) or @(variable/defaultValue)"
                        , "\\@\\(\\s*([\\w_.]+)\\s*(?:\\/\\s*([^)]+)\\s*)?\\)"
                        , true),
                new VariablePattern("#[VAR]"
                        , "#[variable] or #[variable|defaultValue]"
                        , "\\#\\[(\\s*[\\w_.]+)\\s*(?:\\|\\s*([^)]+)\\s*)?\\]"
                        , true),
                new VariablePattern("{{VAR}}"
                        , "{{variable}} or {{variable || \"defaultValue\"}}"
                        , "\\{\\{\\s*([\\w_.]+)\\s*(?:\\|\\|\\s*\"([^\"]+)\")?\\s*\\}\\}"
                        , false),
                new VariablePattern("[(VAR)]"
                        , "[(variable)] or [(variable|defaultValue)]"
                        , "\\[\\(\\s*([\\w_.]+)\\s*(?:\\|\\s*([^)]+))?\\s*\\)\\]"
                        , true));
    }

    public static VariablePattern mapVariablePattern(Map<String, String> map) {
        String variableType = map.get(VARIABLE_PATTERN);
        if (Strings.isNullOrEmpty(variableType) || "NONE".equals(variableType)) {
            return null;
        }
        for (VariablePattern variableTypeChoice : VARIABLE_PATTERN_CHOICES) {
            if (variableTypeChoice.id.equals(variableType)) {
                return variableTypeChoice;
            }
        }
        return null;
    }
}
