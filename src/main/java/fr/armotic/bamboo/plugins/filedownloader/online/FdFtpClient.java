/*
 * Copyright 2016 Armotic
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.armotic.bamboo.plugins.filedownloader.online;

import com.google.common.base.Strings;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.net.ftp.*;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class FdFtpClient {

    private final FTPClient ftpClient;
    private final String host;
    private String user;
    private String password;

    public FdFtpClient(String host) {
        this(host, new FTPClient());
        ftpClient.setBufferSize(1024000);
        FTPClientConfig ftpClientConfig = new FTPClientConfig();
        ftpClient.configure(ftpClientConfig);
    }

    public FdFtpClient(String host, FTPClient ftpClient) {
        this.host = host;
        this.ftpClient = ftpClient;
    }

    public void setCredentials(String user, String password) {
        this.user = user;
        this.password = password;
    }

    public InputStream retrieveFile(String path) throws IOException {
        connect();
        try {
            OutputStream outputStream = new ByteArrayOutputStream();
            ftpClient.retrieveFile(path, outputStream);
            if (ftpClient.getReplyCode() >= 500) {
                return null;
            }
            byte[] content = outputStream.toString().getBytes();
            return new ByteArrayInputStream(content);
        } finally {
            if (ftpClient.isConnected()) {
                ftpClient.disconnect();
            }
        }
    }

    public boolean isFilePresent(String path) throws IOException {
        connect();
        try {
            InputStream inputStream = ftpClient.retrieveFileStream(path);
            int replyCode = ftpClient.getReplyCode();
            if (replyCode >= 300 && replyCode != 550) {
                throw new IOException(ftpClient.getReplyString());
            }
            return inputStream != null && replyCode < 300;
        } finally {
            if (ftpClient.isConnected()) {
                ftpClient.disconnect();
            }
        }
    }

    private void connect() throws IOException {
        ftpClient.connect(host);
        if (!FTPReply.isPositiveCompletion(ftpClient.getReplyCode())) {
            ftpClient.disconnect();
            throw new IOException("FTP server refused connection.");
        }
        if (!Strings.isNullOrEmpty(user)) {
            if (!ftpClient.login(user, password)) {
                throw new IOException("Could not login to FTP server.");
            }
        } else {
            ftpClient.login("anonymous", "");
        }
        ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
        ftpClient.setFileTransferMode(FTP.STREAM_TRANSFER_MODE);
        ftpClient.enterLocalPassiveMode();
        if (ftpClient.getReplyCode() == 500) {
            ftpClient.enterLocalActiveMode();
        }
    }
}
