/*
 * Copyright 2016 Armotic
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.armotic.bamboo.plugins.filedownloader.build;

import com.atlassian.bamboo.repository.RepositoryException;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.vcs.configuration.PlanRepositoryDefinition;
import fr.armotic.bamboo.plugins.filedownloader.core.FdVariableManager;
import fr.armotic.bamboo.plugins.filedownloader.online.FdFtpClient;
import fr.armotic.bamboo.plugins.filedownloader.online.FdFtpsClient;
import org.bouncycastle.util.encoders.Base64;

import java.io.*;
import java.net.URLConnection;

public class FdSupplier {

    private final FdConfig config;
    private final CommonTaskContext taskContext;
    private final FdVariableManager variableManager;

    public FdSupplier(FdConfig config, CommonTaskContext taskContext) {
        this.config = config;
        this.taskContext = taskContext;
        this.variableManager = (FdVariableManager) taskContext
                .getRuntimeTaskData()
                .get("fd-variable-manager");
    }

    public InputStream openStream() throws IOException {
        if ("INLINE".equals(config.locationType)) {
            return retrieveInlineStream();
        } else if ("ONLINE".equals(config.locationType)) {
            return retrieveOnlineStream();
        } else if ("REPOSITORY".equals(config.locationType)) {
            try {
                return retrieveRepositoryStream();
            } catch (RepositoryException e) {
                throw new IOException(e);
            }
        }
        throw new IOException("Unknown location type of file: " + config.locationType);
    }

    public InputStream retrieveInlineStream() {
        ByteArrayInputStream inputStream = new ByteArrayInputStream(config.inlineBody.getBytes());
        return new FdInputStream(inputStream, config, variableManager);
    }

    public InputStream retrieveOnlineStream() throws IOException {
        if (config.onlineLocation.getProtocol().matches("https?")) {
            return retrieveHttpStream();
        } else if (config.onlineLocation.getProtocol().matches("ftps?")) {
            return retrieveFtpStream();
        } else if (config.onlineLocation.getProtocol().equals("file")) {
            return retrieveHttpStream();
        }
        throw new IOException("Unknown protocol: " + config.onlineLocation);
    }

    private InputStream retrieveHttpStream() throws IOException {
        URLConnection connection = config.onlineLocation.openConnection();
        switch (config.authenticationType) {
            case BASIC:
                String userpass = config.login + ":" + config.password;
                String basicAuth = "Basic " + new String(Base64.encode(userpass.getBytes()));
                connection.setRequestProperty("Authorization", basicAuth);
        }
        InputStream inputStream = connection.getInputStream();
        switch (config.fileType) {
            case TEXT:
            case SCRIPT:
                return new FdInputStream(inputStream, config, variableManager);
            case ARCHIVE:
                return inputStream;
            default:
                throw new IOException(config.fileType + " is an unknown type of file.");
        }
    }

    private InputStream retrieveFtpStream() throws IOException {
        FdFtpClient ftpClient;
        if ("ftp".equals(config.onlineLocation.getProtocol())) {
            ftpClient = new FdFtpClient(config.onlineLocation.getHost());
        } else if ("ftps".equals(config.onlineLocation.getProtocol())) {
            ftpClient = new FdFtpsClient(config.onlineLocation.getHost());
        } else {
            throw new RuntimeException("Not a FTP protocol: " + config.onlineLocation.getProtocol());
        }
        switch (config.authenticationType) {
            case BASIC:
                ftpClient.setCredentials(config.login, config.password);
        }
        return ftpClient.retrieveFile(config.onlineLocation.getPath());
    }

    private InputStream retrieveRepositoryStream() throws RepositoryException, FileNotFoundException {
        PlanRepositoryDefinition repository = taskContext.getCommonContext()
                .getVcsRepositoryMap()
                .get(config.repoKey);
        if (repository == null) {
            throw new RepositoryException("Unable to retrieve repository.", config.repoKey);
        }
        FdRepository repo = new FdRepository(repository);
        File sources = new File(taskContext.getWorkingDirectory(), ".fd/" + config.repoKey);
        sources.mkdirs();
        repo.retrieveSourceCode(taskContext.getCommonContext(), sources);
        return new FileInputStream(new File(sources, config.fileOrigin));
    }


}
