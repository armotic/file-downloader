/*
 * Copyright 2016 Armotic
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.armotic.bamboo.plugins.filedownloader;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.plan.PlanHelper;
import com.atlassian.bamboo.plan.cache.ImmutablePlan;
import com.atlassian.bamboo.task.AbstractTaskConfigurator;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.TaskRequirementSupport;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.atlassian.bamboo.v2.build.agent.capability.Requirement;
import com.atlassian.bamboo.vcs.configuration.PlanRepositoryDefinition;
import com.atlassian.core.util.PairType;
import com.atlassian.spring.container.ContainerManager;
import com.atlassian.struts.TextProvider;
import com.google.common.base.Strings;
import fr.armotic.bamboo.plugins.filedownloader.build.FdConfig;
import fr.armotic.bamboo.plugins.filedownloader.core.AuthenticationType;
import fr.armotic.bamboo.plugins.filedownloader.online.FdFtpClient;
import fr.armotic.bamboo.plugins.filedownloader.online.FdFtpsClient;
import org.bouncycastle.util.encoders.Base64;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nullable;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

import static com.atlassian.bamboo.task.TaskContextHelper.getPlan;
import static fr.armotic.bamboo.plugins.filedownloader.build.FdRepository.getRequirements;
import static fr.armotic.bamboo.plugins.filedownloader.FileDownloaderManager.*;
import static fr.armotic.bamboo.plugins.filedownloader.build.BambooVariableShielder.shieldBambooVariables;
import static fr.armotic.bamboo.plugins.filedownloader.build.BambooVariableShielder.unshieldBambooVariables;

public class FileDownloaderConfigurator extends AbstractTaskConfigurator implements TaskRequirementSupport {

    @Override
    @NotNull
    public Map<String, String> generateTaskConfigMap(
            @NotNull final ActionParametersMap params,
            @Nullable final TaskDefinition previousTaskDefinition) {
        final Map<String, String> config = super.generateTaskConfigMap(params, previousTaskDefinition);
        config.put(FILE_TYPE, params.getString(FILE_TYPE));
        config.put(VARIABLE_PATTERN, params.getString(VARIABLE_PATTERN));
        config.put(ARCHIVE_EXTRACTION, String.valueOf(params.getBoolean(ARCHIVE_EXTRACTION)));

        config.put(LOCATION_TYPE, params.getString(LOCATION_TYPE));
        config.put(INLINE_BODY, shieldBambooVariables(params.getString(INLINE_BODY)));
        config.put(LINE_ENDING, params.getString(LINE_ENDING));
        config.put(ONLINE_LOCATION, params.getString(ONLINE_LOCATION));
        config.put(ONLINE_TEST, params.getString(ONLINE_TEST));
        config.put(AUTHENTICATION, params.getString(AUTHENTICATION));
        config.put(LOGIN, params.getString(LOGIN));
        if (!Strings.isNullOrEmpty(params.getString(PASSWORD))) {
            config.put(PASSWORD, params.getString(PASSWORD));
        } else if (previousTaskDefinition != null) {
            config.put(PASSWORD, previousTaskDefinition.getConfiguration().get(PASSWORD));
        }
        config.put(REPOSITORY, params.getString(REPOSITORY));
        config.put(FILE_ORIGIN, params.getString(FILE_ORIGIN));

        config.put(FILE_DESTINATION, params.getString(FILE_DESTINATION));
        return config;
    }

    @Override
    public void populateContextForCreate(
            @NotNull final Map<String, Object> context
    ) {
        super.populateContextForCreate(context);
        context.put(IS_CREATION, true);

        context.put(FILE_TYPE, "TEXT");
        context.put(VARIABLE_PATTERN, "NONE");
        context.put(ARCHIVE_EXTRACTION, false);

        context.put(LOCATION_TYPE, "INLINE");
        context.put(INLINE_BODY, "");
        context.put(LINE_ENDING, "LF");
        context.put(ONLINE_LOCATION, "https://");
        context.put(ONLINE_TEST, true);
        context.put(AUTHENTICATION, "NONE");
        context.put(LOGIN, "");
        context.put(PASSWORD, "");
        context.put(REPOSITORY, "");
        context.put(FILE_ORIGIN, "");

        context.put(FILE_DESTINATION, "");


        context.put(FILE_TYPES, FILE_TYPE_CHOICES);
        context.put(VARIABLE_PATTERNS, VARIABLE_PATTERN_CHOICES);
        context.put(AUTHENTICATIONS, AUTHENTICATION_CHOICES);
        context.put(LINE_ENDINGS, LINE_ENDING_CHOICES);
        List<PairType> repositories = retrieveRepositories(context);
        context.put(LOCATION_TYPES, repositories.isEmpty()
                ? LOCATION_TYPE_CHOICES_WITHOUT_REPOSITORY
                : LOCATION_TYPE_CHOICES);
        context.put(REPOSITORIES, repositories);
    }

    @Override
    public void populateContextForEdit(
            @NotNull final Map<String, Object> context,
            @NotNull final TaskDefinition taskDefinition
    ) {
        super.populateContextForEdit(context, taskDefinition);
        context.put(IS_CREATION, false);

        context.put(FILE_TYPE, taskDefinition.getConfiguration().get(FILE_TYPE));
        context.put(VARIABLE_PATTERN, taskDefinition.getConfiguration().get(VARIABLE_PATTERN));
        context.put(ARCHIVE_EXTRACTION, taskDefinition.getConfiguration().get(ARCHIVE_EXTRACTION));

        context.put(LOCATION_TYPE, taskDefinition.getConfiguration().get(LOCATION_TYPE));
        context.put(INLINE_BODY, unshieldBambooVariables(taskDefinition.getConfiguration().get(INLINE_BODY)));
        context.put(LINE_ENDING, taskDefinition.getConfiguration().get(LINE_ENDING));
        context.put(ONLINE_LOCATION, taskDefinition.getConfiguration().get(ONLINE_LOCATION));
        context.put(ONLINE_TEST, false);
        String authenticationType = taskDefinition.getConfiguration().get(AUTHENTICATION);
        context.put(AUTHENTICATION, authenticationType);
        if ("NONE".equals(authenticationType)) {
            context.put(LOGIN, "");
            context.put(PASSWORD, "");
        } else {
            context.put(LOGIN, taskDefinition.getConfiguration().get(LOGIN));
            context.put(PASSWORD, taskDefinition.getConfiguration().get(PASSWORD));
        }
        context.put(REPOSITORY, taskDefinition.getConfiguration().get(REPOSITORY));
        context.put(FILE_ORIGIN, taskDefinition.getConfiguration().get(FILE_ORIGIN));

        context.put(FILE_DESTINATION, taskDefinition.getConfiguration().get(FILE_DESTINATION));


        context.put(FILE_TYPES, FILE_TYPE_CHOICES);
        context.put(VARIABLE_PATTERNS, VARIABLE_PATTERN_CHOICES);
        context.put(LOCATION_TYPES, LOCATION_TYPE_CHOICES);
        context.put(AUTHENTICATIONS, AUTHENTICATION_CHOICES);
        context.put(LINE_ENDINGS, LINE_ENDING_CHOICES);
        List<PairType> repositories = retrieveRepositories(context);
        context.put(LOCATION_TYPES, repositories.isEmpty()
                ? LOCATION_TYPE_CHOICES_WITHOUT_REPOSITORY
                : LOCATION_TYPE_CHOICES);
        context.put(REPOSITORIES, repositories);
    }

    private List<PairType> retrieveRepositories(Map<String, Object> context) {
        ArrayList<PairType> availableRepositories = new ArrayList<>();
        ImmutablePlan plan = getPlan(context);
        if (plan == null) {
            return availableRepositories;
        }
        LinkedHashMap<Long, PlanRepositoryDefinition> repositories = PlanHelper
                .getPlanRepositoryDefinitionMap(plan);
        for (PlanRepositoryDefinition repository : repositories.values()) {
            availableRepositories.add(new PairType(
                    repository.getId() + ":" + repository.getPluginKey(),
                    repository.getName()));
        }
        return availableRepositories;
    }

    @Override
    public void validate(
            @NotNull final ActionParametersMap params,
            @NotNull final ErrorCollection errorCollection
    ) {
        super.validate(params, errorCollection);
        TextProvider textProvider = ContainerManager.getComponent("textProvider", TextProvider.class);

        if (!"ARCHIVE".equals(params.getString(FILE_TYPE))
                && "true".equals(params.getString(ARCHIVE_EXTRACTION))) {
            errorCollection.addError(ARCHIVE_EXTRACTION,
                    textProvider.getText("f.a.b.p.f.error.extract.textfile"));
        }

        String url = params.getString(ONLINE_LOCATION);
        if ("ONLINE".equals(params.getString(LOCATION_TYPE))
                && Strings.isNullOrEmpty(url)) {
            errorCollection.addError(ONLINE_LOCATION,
                    textProvider.getText("f.a.b.p.f.error.onlinelocation.missing"));
        }
        if ("ONLINE".equals(params.getString(LOCATION_TYPE))
                && !Strings.isNullOrEmpty(url)) {
            try {
                validateOnlineLocation(params, errorCollection, textProvider);
            } catch (MalformedURLException e) {
                errorCollection.addError(ONLINE_LOCATION,
                        textProvider.getText("f.a.b.p.f.error.onlinelocation.badurl"));
            } catch (Exception e) {
                errorCollection.addError(ONLINE_LOCATION,
                        textProvider.getText("f.a.b.p.f.error.onlinelocation.validation")
                                + " " + e.getMessage());
            }
        }

        String fileDestination = params.getString(FILE_DESTINATION);
        if (Strings.isNullOrEmpty(fileDestination)) {
            errorCollection.addError(FILE_DESTINATION,
                    textProvider.getText("f.a.b.p.f.error.destination.missing"));
        } else if (fileDestination.matches("[^+$><,~=\"?!%#*|]")) {
            errorCollection.addError(FILE_DESTINATION,
                    textProvider.getText("f.a.b.p.f.error.destination.incorrect"));
        }
    }

    private void validateOnlineLocation(@NotNull ActionParametersMap params,
                                        @NotNull ErrorCollection errorCollection,
                                        TextProvider textProvider)
            throws MalformedURLException {
        String onlineLocation = params.getString(ONLINE_LOCATION);
        if (Strings.isNullOrEmpty(onlineLocation)) {
            errorCollection.addError(ONLINE_LOCATION,
                    textProvider.getText("f.a.b.p.f.error.onlinelocation.badurl"));
            return;
        }
        URL location = new URL(onlineLocation);
        if (location.getProtocol().matches("https?")
                || "file".equals(location.getProtocol())) {
            if (params.getBoolean(ONLINE_TEST)) {
                validateHttp(params, errorCollection, textProvider);
            }
        } else if (location.getProtocol().matches("ftps?")) {
            if (params.getBoolean(ONLINE_TEST)) {
                validateFtp(params, errorCollection, textProvider);
            }
        } else {
            errorCollection.addError(ONLINE_LOCATION,
                    textProvider.getText("f.a.b.p.f.error.onlinelocation.badprotocol"));
        }
    }

    private void validateHttp(@NotNull ActionParametersMap params,
                              @NotNull ErrorCollection errorCollection,
                              TextProvider textProvider) {
        String onlineLocation = params.getString(ONLINE_LOCATION);
        if (Strings.isNullOrEmpty(onlineLocation)) {
            errorCollection.addError(ONLINE_LOCATION,
                    textProvider.getText("f.a.b.p.f.error.onlinelocation.badurl"));
            return;
        }
        try {
            URL url = new URL(onlineLocation);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            HttpURLConnection.setFollowRedirects(false);
            AuthenticationType authenticationType = AuthenticationType.valueOf(params.getString(AUTHENTICATION));
            switch (authenticationType) {
                case BASIC:
                    if (Strings.isNullOrEmpty(params.getString(LOGIN))) {
                        errorCollection.addError(LOGIN,
                                textProvider.getText("f.a.b.p.f.error.login.empty"));
                    }
                    if (Strings.isNullOrEmpty(params.getString(PASSWORD))) {
                        if (params.getBoolean(IS_CREATION)) {
                            errorCollection.addError(PASSWORD,
                                    textProvider.getText("f.a.b.p.f.error.password.empty"));
                        } else {
                            return;
                        }
                    }
                    String userpass = params.getString(LOGIN) + ":" + params.getString(PASSWORD);
                    String basicAuth = "Basic " + new String(Base64.encode(userpass.getBytes()));
                    connection.setRequestProperty("Authorization", basicAuth);
            }
            connection.setRequestMethod("HEAD");
            int responseCode = connection.getResponseCode();
            if (responseCode != HttpURLConnection.HTTP_OK) {
                switch (responseCode) {
                    case 401:
                        errorCollection.addError(ONLINE_LOCATION, "Error " + responseCode + ": " +
                                textProvider.getText("f.a.b.p.f.error.onlinelocation.unauthorized"));
                        break;
                    case 404:
                        errorCollection.addError(ONLINE_LOCATION, "Error " + responseCode + " " +
                                textProvider.getText("f.a.b.p.f.error.onlinelocation.notfound"));
                        break;
                    default:
                        errorCollection.addError(ONLINE_LOCATION, "Error " + responseCode + " " +
                                textProvider.getText("f.a.b.p.f.error.onlinelocation.default"));
                }
            }
        } catch (Exception e) {
            errorCollection.addError(ONLINE_LOCATION,
                    textProvider.getText("f.a.b.p.f.error.onlinelocation.notaccessible")
                            + " " + e.getMessage());
        }
    }

    private void validateFtp(@NotNull ActionParametersMap params,
                             @NotNull ErrorCollection errorCollection,
                             TextProvider textProvider)
            throws MalformedURLException {
        String onlineLocation = params.getString(ONLINE_LOCATION);
        if (Strings.isNullOrEmpty(onlineLocation)) {
            errorCollection.addError(ONLINE_LOCATION,
                    textProvider.getText("f.a.b.p.f.error.onlinelocation.badurl"));
            return;
        }
        URL location = new URL(onlineLocation);
        FdFtpClient ftpClient;
        if ("ftp".equals(location.getProtocol())) {
            ftpClient = new FdFtpClient(location.getHost());
        } else if ("ftps".equals(location.getProtocol())) {
            ftpClient = new FdFtpsClient(location.getHost());
        } else {
            throw new RuntimeException("Not a FTP protocol: " + location.getProtocol());
        }
        AuthenticationType authenticationType = AuthenticationType.valueOf(params.getString(AUTHENTICATION));
        switch (authenticationType) {
            case BASIC:
                if (Strings.isNullOrEmpty(params.getString(LOGIN))) {
                    errorCollection.addError(LOGIN,
                            textProvider.getText("f.a.b.p.f.error.login.empty"));
                }
                if (Strings.isNullOrEmpty(params.getString(PASSWORD))) {
                    if (params.getBoolean(IS_CREATION)) {
                        errorCollection.addError(PASSWORD,
                                textProvider.getText("f.a.b.p.f.error.password.empty"));
                    } else {
                        return;
                    }
                }
                ftpClient.setCredentials(params.getString(LOGIN), params.getString(PASSWORD));
        }
        try {
            if (!ftpClient.isFilePresent(location.getPath())) {
                errorCollection.addError(ONLINE_LOCATION,
                        textProvider.getText("f.a.b.p.f.error.onlinelocation.notfound")
                                + location.getPath());
            }
        } catch (Exception e) {
            errorCollection.addError(ONLINE_LOCATION,
                    textProvider.getText("f.a.b.p.f.error.onlinelocation.notaccessible")
                            + " " + e.getMessage());
        }
    }

    @NotNull
    @Override
    public Set<Requirement> calculateRequirements(@NotNull TaskDefinition taskDefinition) {
        FdConfig config = new FdConfig(taskDefinition.getConfiguration());
        return getRequirements(config.repoType);
    }
}
