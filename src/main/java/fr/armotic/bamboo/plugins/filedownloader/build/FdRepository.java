/*
 * Copyright 2016 Armotic
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.armotic.bamboo.plugins.filedownloader.build;

import com.atlassian.bamboo.plan.branch.VcsBranch;
import com.atlassian.bamboo.plan.vcsRevision.PlanVcsRevisionData;
import com.atlassian.bamboo.repository.RepositoryException;
import com.atlassian.bamboo.v2.build.CommonContext;
import com.atlassian.bamboo.v2.build.agent.capability.Requirement;
import com.atlassian.bamboo.v2.build.agent.capability.RequirementImpl;
import com.atlassian.bamboo.vcs.configuration.PlanRepositoryDefinition;
import com.atlassian.bamboo.vcs.configuration.VcsBranchDefinition;
import com.atlassian.bamboo.vcs.module.VcsRepositoryManager;
import com.atlassian.bamboo.vcs.module.VcsRepositoryModuleDescriptor;
import com.atlassian.bamboo.vcs.runtime.VcsWorkingCopyManager;
import com.atlassian.spring.container.ContainerManager;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import static java.util.Arrays.asList;
import static java.util.Optional.of;
import static java.util.Optional.ofNullable;

public class FdRepository {

    private final PlanRepositoryDefinition repository;

    public FdRepository(PlanRepositoryDefinition repository) {
        this.repository = repository;
    }

    public void retrieveSourceCode(CommonContext buildContext, File workspace) throws RepositoryException {
        VcsBranch branch = of(this.repository)
                .map(PlanRepositoryDefinition::getBranch)
                .map(VcsBranchDefinition::getVcsBranch)
                .orElseThrow(() -> new RepositoryException("Cannot get branch of repository.", repository.getId()));
        PlanVcsRevisionData revision = new PlanVcsRevisionData(
                branch.getName(), null, branch);
        getWorkingCopyManager().retrieveSourceCode(buildContext, this.repository, revision, workspace);
    }

    public VcsWorkingCopyManager getWorkingCopyManager() throws RepositoryException {
        return ofNullable(ContainerManager.getComponent("vcsRepositoryManager"))
                .map(VcsRepositoryManager.class::cast)
                .map(manager -> manager.getVcsRepositoryModuleDescriptor(repository.getPluginKey()))
                .map(VcsRepositoryModuleDescriptor::getWorkingCopyManager)
                .orElseThrow(() -> new RepositoryException("Failed to get WorkingCopyManager of repository.", repository.getId()));
    }

    @NotNull
    public static Set<Requirement> getRequirements(String repoType) {
        Requirement gitRequirement = new RequirementImpl("system.git.executable", true, ".*");
        Requirement svnRequirement = new RequirementImpl("system.svn.executable", true, ".*");
        Requirement hgRequirement = new RequirementImpl("system.hg.executable", true, ".*");
        HashSet<Requirement> set = new HashSet<>();
        if (asList("bb", "gh", "git").contains(repoType)) {
            set.add(gitRequirement);
        }
        if ("svn".equals(repoType)) {
            set.add(svnRequirement);
        }
        if ("hg".equals(repoType)) {
            set.add(hgRequirement);
        }
        return set;
    }
}
