/*
 * Copyright 2016 Armotic
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.armotic.bamboo.plugins.filedownloader.build;

import com.atlassian.bamboo.task.CommonTaskContext;
import com.google.common.base.Strings;
import fr.armotic.bamboo.plugins.filedownloader.FileDownloaderManager;
import fr.armotic.bamboo.plugins.filedownloader.core.AuthenticationType;
import fr.armotic.bamboo.plugins.filedownloader.core.EolType;
import fr.armotic.bamboo.plugins.filedownloader.core.FileType;
import fr.armotic.bamboo.plugins.filedownloader.core.VariablePattern;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import static com.google.common.base.Strings.nullToEmpty;
import static fr.armotic.bamboo.plugins.filedownloader.FileDownloaderManager.*;
import static fr.armotic.bamboo.plugins.filedownloader.build.BambooVariableShielder.unshieldBambooVariables;
import static fr.armotic.bamboo.plugins.filedownloader.core.AuthenticationType.NONE;

public class FdConfig {
    public FileType fileType;
    public VariablePattern variableType;
    public boolean archiveExtraction;

    public String locationType;
    public String inlineBody;
    public EolType lineEnding;
    public URL onlineLocation;
    public AuthenticationType authenticationType;
    public String login;
    public String password;
    public long repoKey;
    public String repoPlugin;
    public String repoType;
    public String fileOrigin;

    public String fileDestination;

    public FdConfig() {
        this.fileType = FileType.TEXT;
        this.authenticationType = NONE;
        this.lineEnding = EolType.LF;
    }

    public FdConfig(Map<String, String> map) {
        this.fileType = FileType.valueOf(map.get(FILE_TYPE));
        this.variableType = FileDownloaderManager.mapVariablePattern(map);
        this.archiveExtraction = Boolean.parseBoolean(map.get(ARCHIVE_EXTRACTION));

        this.locationType = map.get(LOCATION_TYPE);
        this.inlineBody = unshieldBambooVariables(map.get(INLINE_BODY));
        this.lineEnding = EolType.valueOf(map.get(LINE_ENDING));
        String onlineLocationUrl = map.get(ONLINE_LOCATION);
        try {
            if (!Strings.isNullOrEmpty(onlineLocationUrl)) {
                this.onlineLocation = new URL(onlineLocationUrl);
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("This is not a valid URL: " + onlineLocationUrl, e);
        }
        this.authenticationType = AuthenticationType.valueOfOrDefault(map.get(AUTHENTICATION), NONE);
        this.login = map.get(LOGIN);
        this.password = map.get(PASSWORD);
        String[] repoDescription = nullToEmpty(map.get(REPOSITORY)).split(":");
        if (repoDescription[0].matches("\\d+")) {
            this.repoKey = Long.valueOf(repoDescription[0]);
        }
        if (repoDescription.length > 1) {
            this.repoPlugin = repoDescription[1];
        }
        if (repoDescription.length > 2) {
            this.repoType = repoDescription[2];
        }
        this.fileOrigin = map.get(FILE_ORIGIN);

        this.fileDestination = map.get(FILE_DESTINATION);
    }

    public FdConfig(CommonTaskContext taskContext) {
        this(taskContext.getConfigurationMap());
    }
}
