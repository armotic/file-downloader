/*
 * Copyright 2016 Armotic
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.armotic.bamboo.plugins.filedownloader;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.task.*;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import fr.armotic.bamboo.plugins.filedownloader.build.FdConfig;
import fr.armotic.bamboo.plugins.filedownloader.build.FdSupplier;
import fr.armotic.bamboo.plugins.filedownloader.build.VariableManager;
import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.ArchiveException;
import org.apache.commons.compress.archivers.ArchiveInputStream;
import org.apache.commons.compress.archivers.ArchiveStreamFactory;
import org.apache.commons.compress.compressors.CompressorException;
import org.apache.commons.compress.compressors.CompressorStreamFactory;
import org.jetbrains.annotations.NotNull;

import java.io.*;

import static fr.armotic.bamboo.plugins.filedownloader.core.FileType.SCRIPT;

@Scanned
public class FileDownloaderTask implements CommonTaskType {

    @NotNull
    @Override
    public TaskResult execute(@NotNull final CommonTaskContext taskContext) throws TaskException {
        final FdConfig config = new FdConfig(taskContext);
        taskContext.getRuntimeTaskData().put("fd-variable-manager", new VariableManager());
        final BuildLogger buildLogger = taskContext.getBuildLogger();

        File workingDirectory = taskContext.getWorkingDirectory();
        buildLogger.addBuildLogEntry("Destination Folder should be: " + workingDirectory);
        final File outputFile;
        if (config.fileDestination.matches("^([A-Za-z]:)?(\\\\|//).*")) {
            outputFile = new File(config.fileDestination);
        } else {
            outputFile = new File(workingDirectory, config.fileDestination);
        }

        if (removeFileIfExists(config, buildLogger, outputFile)) {
            return TaskResultBuilder.newBuilder(taskContext).failedWithError().build();
        }

        if ("INLINE".equals(config.locationType)) {
            buildLogger.addBuildLogEntry("Copying inline file into " + config.fileDestination);
        } else if ("ONLINE".equals(config.locationType)) {
            buildLogger.addBuildLogEntry("Downloading: " + config.onlineLocation + " into " + config.fileDestination);
        } else if ("REPOSITORY".equals(config.locationType)) {
            buildLogger.addBuildLogEntry("Retrieving: " + config.fileOrigin + " into " + config.fileDestination);
        }

        FdSupplier supplier = new FdSupplier(config, taskContext);
        writeIntoLocation(config, supplier, outputFile);
        buildLogger.addBuildLogEntry("New file successfully downloaded at " + outputFile.getAbsolutePath()
                + " and weighs " + outputFile.length());

        if (config.fileType == SCRIPT) {
            if (outputFile.setExecutable(true)) {
                buildLogger.addBuildLogEntry("File is now executable.");
            }
        }

        return TaskResultBuilder.newBuilder(taskContext).success().build();
    }

    boolean removeFileIfExists(FdConfig config, BuildLogger buildLogger, File outputFile) throws TaskException {
        if (outputFile.isFile()) {
            buildLogger.addBuildLogEntry("Removing " + config.fileDestination);
            if (!outputFile.delete()) {
                buildLogger.addBuildLogEntry("Failed to remove " + config.fileDestination);
                return true;
            }
        }
        return false;
    }

    void writeIntoLocation(FdConfig config, FdSupplier supplier, File outputFile)
            throws TaskException {
        try{
            InputStream input = new BufferedInputStream(supplier.openStream());
            if (config.archiveExtraction) {
                input = decompressInputStream(input);
                unpackInputStream(input, outputFile);
            } else {
                writeIntoLocation(input, outputFile);
            }
        } catch (IOException | ArchiveException e) {
            throw new TaskException(e.getMessage(), e);
        }
    }

    @NotNull
    private InputStream decompressInputStream(InputStream input) {
        try {
            InputStream compressedInput = new CompressorStreamFactory().createCompressorInputStream(input);
            return new BufferedInputStream(compressedInput);
        } catch (CompressorException e) {
            return input;
        }
    }

    private void unpackInputStream(InputStream input, File outputFile) throws ArchiveException, IOException {
        ArchiveInputStream archiveInputStream = new ArchiveStreamFactory().createArchiveInputStream(input);
        ArchiveEntry archiveEntry = archiveInputStream.getNextEntry();
        while (archiveEntry != null) {
            if (archiveEntry.isDirectory()) {
                File newDirectory = new File(outputFile, archiveEntry.getName());
                newDirectory.mkdirs();
            } else {
                File newFile = new File(outputFile, archiveEntry.getName());
                writeIntoLocation(archiveInputStream, newFile);
            }
            archiveEntry = archiveInputStream.getNextEntry();
        }
    }

    void writeIntoLocation(InputStream inputStream, File outputFile) throws IOException {
        outputFile.getParentFile().mkdirs();
        OutputStream outStream = new FileOutputStream(outputFile);
        try {
            byte[] buffer = new byte[8 * 1024];
            int bytesRead;
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outStream.write(buffer, 0, bytesRead);
            }
        } finally {
            outStream.close();
        }
    }
}
