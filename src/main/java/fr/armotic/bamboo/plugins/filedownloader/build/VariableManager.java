/*
 * Copyright 2016 Armotic
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.armotic.bamboo.plugins.filedownloader.build;

import com.atlassian.bamboo.variable.CustomVariableContext;
import com.atlassian.bamboo.variable.VariableDefinitionContext;
import com.atlassian.spring.container.ContainerManager;
import fr.armotic.bamboo.plugins.filedownloader.core.FdVariableManager;

import javax.annotation.Nullable;
import java.util.Map;


public class VariableManager implements FdVariableManager {
    public Map<String, VariableDefinitionContext> map;

    public VariableManager() {
        this.map = ContainerManager
                .getComponent("customVariableContext", CustomVariableContext.class)
                .getVariableContexts();
    }

    @Override
    public String getValue(String key) throws NullPointerException {
        VariableDefinitionContext variable = findValue(key);
        if (variable == null && key.startsWith("bamboo.")) {
            variable = findValue(key.substring(7));
        }
        if (variable == null) {
            throw new NullPointerException(key + " does not exist");
        }
        return variable.getValue();
    }

    @Override
    public String getValueOrDefault(String key, String defaultValue) {
        VariableDefinitionContext variable = findValue(key);
        if (variable == null) {
            return defaultValue;
        }
        return variable.getValue();
    }

    @Nullable
    private VariableDefinitionContext findValue(String key) {
        VariableDefinitionContext variable = this.map.get(key);
        if (variable == null) {
            if (key.startsWith("bamboo.")) {
                return findValue(key.substring(7));
            }
        }
        return variable;
    }
}
