/*
 * Copyright 2016 Armotic
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.armotic.bamboo.plugins.filedownloader.build;

import com.google.common.base.Strings;
import fr.armotic.bamboo.plugins.filedownloader.core.EolType;
import fr.armotic.bamboo.plugins.filedownloader.core.FdVariableManager;
import fr.armotic.bamboo.plugins.filedownloader.core.VariablePattern;

import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FdInputStream extends InputStream {

    private final InputStream source;
    private LinkedList<Integer> line;
    private EolType eol;
    private VariablePattern variablePattern;
    private FdVariableManager variableManager;
    private boolean lastCharWasCR;
    private boolean isClosed;

    public FdInputStream(InputStream inputStream,
                         FdConfig config,
                         FdVariableManager variableManager) {
        this.source = inputStream;
        this.line = new LinkedList<Integer>();
        this.eol = config.lineEnding;
        this.variablePattern = config.variableType;
        this.variableManager = variableManager;
    }

    @Override
    public int available ()throws IOException {
        if (line.isEmpty()) {
            return 0;
        }
        return 1;
    }

    @Override
    public int read ()throws IOException {
        if (line.isEmpty() && !nextLine()) {
            return -1;
        }
        return line.pop();
    }

    protected boolean nextLine() throws IOException {
        if (isClosed) {
            return false;
        }
        int nextByte = source.read();
        if (lastCharWasCR && nextByte == '\n') {
            nextByte = source.read();
        }
        if (nextByte < 0) {
            return false;
        }
        StringBuilder buildingNewline = new StringBuilder();
        while (nextByte > 0 && nextByte != '\n')
        {
            buildingNewline.append((char)nextByte);
            nextByte = source.read();
            if (nextByte == '\r') {
                lastCharWasCR = true;
                break;
            }
        }
        String newline = checkForVariables(buildingNewline.toString());
        for (byte character : newline.getBytes()) {
            line.add((int) character);
        }
        if (nextByte < 0) {
            return true;
        }
        switch (eol) {
            case CR:
                line.add((int) '\r');
                break;
            case CRLF:
                line.add((int) '\r');
            case LF:
                line.add((int) '\n');
        }
        return true;
    }

    protected String checkForVariables(String newline) {
        if (variablePattern == null) {
            return newline;
        }
        Matcher matcher = variablePattern.getPattern().matcher(newline);
        while(matcher.find()) {
            String value;
            String defaultValue = matcher.group(2);
            if (Strings.isNullOrEmpty(defaultValue)) {
                value = variableManager.getValue(matcher.group(1));
            } else {
                value = variableManager.getValueOrDefault(matcher.group(1), defaultValue);
            }
            if (variablePattern.hasToBeTrimmed()) {
                value = value.trim();
            }
            newline = newline.replace(matcher.group(0), value);
        }
        return newline;
    }

    @Override
    public void close() throws IOException {
        super.close();
        isClosed = true;
    }
}
