/*
 * Copyright 2016 Armotic
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.armotic.bamboo.plugins.filedownloader;

import com.atlassian.bamboo.serialization.WhitelistedSerializable;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.task.TaskException;
import fr.armotic.bamboo.plugins.filedownloader.core.AuthenticationType;
import fr.armotic.bamboo.plugins.filedownloader.core.FileType;
import fr.armotic.bamboo.plugins.filedownloader.build.FdConfig;
import fr.armotic.bamboo.plugins.filedownloader.build.FdSupplier;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.*;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;

public class FileDownloaderTaskTest {

    private FileDownloaderTask task;
    private File testArchive;
    private FdConfig config;
    private FdSupplier supplier;
    private FakeVariableManager variableManager;

    @Mock
    TaskContext taskContext;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        this.task = new FileDownloaderTask();
        this.config = new FdConfig();
        this.config.authenticationType = AuthenticationType.NONE;
        this.variableManager = new FakeVariableManager();
        HashMap<String, WhitelistedSerializable> taskData = new HashMap<String, WhitelistedSerializable>();
        taskData.put("fd-variable-manager", this.variableManager);
        doReturn(taskData)
                .when(taskContext)
                .getRuntimeTaskData();
        this.supplier = new FdSupplier(this.config, taskContext);
        this.testArchive = new File(System.getProperty("java.io.tmpdir"), "archiveDownload-" + UUID.randomUUID());
        removeOutputTestArchive();
    }

    @After
    public void tearDown() throws Exception {
        removeOutputTestArchive();
    }

    private void removeOutputTestArchive() throws IOException {
        if (testArchive.isFile()) {
            if (!testArchive.delete()) {
                testArchive.deleteOnExit();
            }
        }
        if (testArchive.isDirectory()) {
            FileUtils.deleteDirectory(testArchive);
        }
    }

    @Test
    public void writeIntoLocation_simpleArchive() throws Exception {
        this.config.onlineLocation = getClass().getClassLoader().getResource("testArchiveDownload.zip");
        this.config.fileType = FileType.ARCHIVE;
        this.config.locationType = "ONLINE";

        task.writeIntoLocation(config, supplier, testArchive);

        assertEquals("Output should have been a zip file.", testArchive.isFile(), true);
        ZipFile zipFile = new ZipFile(testArchive);
        Enumeration<? extends ZipEntry> entries = zipFile.entries();
        while(entries.hasMoreElements()){
            ZipEntry entry = entries.nextElement();
            InputStream inputStream = zipFile.getInputStream(entry);
            String content = IOUtils.toString(inputStream, "UTF-8");
            inputStream.close();
            assertThat(content, equalTo("This test worked."));
        }
        zipFile.close();
        assertThat(testArchive.length(), equalTo(171L));
    }

    @Test(expected = TaskException.class)
    public void writeIntoLocation_Extracting7zArchive_shouldFail() throws Exception {
        testExtraction("testArchiveDownload.7z");
    }

    @Test
    public void writeIntoLocation_ExtractingBz2Archive() throws Exception {
        testExtraction("testArchiveDownload.tar.bz2");
    }

    @Test
    public void writeIntoLocation_ExtractingGzArchive() throws Exception {
        testExtraction("testArchiveDownload.tar.gz");
    }

    @Test
    public void writeIntoLocation_ExtractingXzArchive() throws Exception {
        testExtraction("testArchiveDownload.tar.xz");
    }

    @Test
    public void writeIntoLocation_ExtractingZipArchive() throws Exception {
        testExtraction("testArchiveDownload.zip");
    }

    private void testExtraction(String archiveName) throws TaskException, IOException {
        this.config.onlineLocation = getClass().getClassLoader().getResource(archiveName);
        this.config.fileType = FileType.ARCHIVE;
        this.config.archiveExtraction = true;
        this.config.locationType = "ONLINE";

        task.writeIntoLocation(config, supplier, testArchive);

        assertEquals("Output should have been a directory.", testArchive.isDirectory(), true);

        File sample = new File(testArchive, "sample.txt");
        InputStream inputStream = new FileInputStream(sample);
        String content = IOUtils.toString(inputStream, "UTF-8");
        inputStream.close();
        assertThat(content, equalTo("This test worked."));
    }
}