/*
 * Copyright 2016 Armotic
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.armotic.bamboo.plugins.filedownloader.build;

import fr.armotic.bamboo.plugins.filedownloader.FakeVariableManager;
import org.junit.Before;
import org.junit.Test;

import static fr.armotic.bamboo.plugins.filedownloader.FileDownloaderManager.VARIABLE_PATTERN_CHOICES;
import static org.junit.Assert.assertEquals;

public class FdInputStreamTest {

    private FakeVariableManager variableManager;
    private FdInputStream fdInputStream;
    private FdConfig config;

    @Before
    public void setUp() throws Exception {
        variableManager = new FakeVariableManager();
        config = new FdConfig();
        config.variableType = VARIABLE_PATTERN_CHOICES.get(5);
        fdInputStream = new FdInputStream(null, config, variableManager);
    }

    @Test
    public void checkForVariables_toReplaceWithPath() throws Exception {
        config.variableType = VARIABLE_PATTERN_CHOICES.get(5);
        variableManager.valueMap.put("path", "C:\\Windows\\Path\\example.txt");
        String result = fdInputStream.checkForVariables("In my example, the path should be '{{path}}'");

        assertEquals("In my example, the path should be 'C:\\Windows\\Path\\example.txt'", result);
    }

    @Test
    public void checkForVariables_withMultipleVariables() throws Exception {
        config.variableType = VARIABLE_PATTERN_CHOICES.get(5);
        variableManager.valueMap.put("one", "one");
        variableManager.valueMap.put("two", "two");
        String result = fdInputStream.checkForVariables("{{one}} + {{one}} = {{two}}");

        assertEquals("one + one = two", result);
    }
}