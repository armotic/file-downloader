/*
 * Copyright 2016 Armotic
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.armotic.bamboo.plugins.filedownloader.build;


import org.junit.Test;

import static fr.armotic.bamboo.plugins.filedownloader.build.BambooVariableShielder.shieldBambooVariables;
import static fr.armotic.bamboo.plugins.filedownloader.build.BambooVariableShielder.unshieldBambooVariables;
import static org.junit.Assert.assertEquals;

public class BambooVariableShielderTest {


    @Test
    public void shielding() throws Exception {
        String result = shieldBambooVariables("${bamboo.buildKey}");
        assertEquals("<fd-var:bamboo.buildKey>", result);
    }

    @Test
    public void unshielding() throws Exception {
        String result = unshieldBambooVariables("<fd-var:bamboo.buildKey>");
        assertEquals("${bamboo.buildKey}", result);
    }
}