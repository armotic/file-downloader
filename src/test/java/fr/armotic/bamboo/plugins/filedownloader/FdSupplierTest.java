/*
 * Copyright 2016 Armotic
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.armotic.bamboo.plugins.filedownloader;

import com.atlassian.bamboo.serialization.WhitelistedSerializable;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.task.TaskContextHelper;
import fr.armotic.bamboo.plugins.filedownloader.core.EolType;
import fr.armotic.bamboo.plugins.filedownloader.build.FdConfig;
import fr.armotic.bamboo.plugins.filedownloader.build.FdSupplier;
import org.apache.commons.io.IOUtils;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import static fr.armotic.bamboo.plugins.filedownloader.FileDownloaderManager.VARIABLE_PATTERN_CHOICES;
import static org.junit.Assert.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FdSupplierTest {

    private FdConfig config;
    private FdSupplier supplier;
    private FakeVariableManager variableManager;

    @Mock
    TaskContext taskContext;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        this.config = new FdConfig();
        this.variableManager = new FakeVariableManager();
        HashMap<String, WhitelistedSerializable> taskData = new HashMap<String, WhitelistedSerializable>();
        taskData.put("fd-variable-manager", this.variableManager);
        doReturn(taskData)
                .when(taskContext)
                .getRuntimeTaskData();
        this.supplier = new FdSupplier(this.config, taskContext);
    }

    @Test
    public void retrieveInlineStream_withLF_asEOL() throws Exception {
        this.config.lineEnding = EolType.LF;
        testInlineStream(
                "A\nB\r\nC\rD",
                "A\nB\nC\nD");
    }

    @Test
    public void retrieveInlineStream_withCRLF_asEOL() throws Exception {
        this.config.lineEnding = EolType.CRLF;
        testInlineStream(
                "A\nB\r\nC\rD",
                "A\r\nB\r\nC\r\nD");
    }

    @Test
    public void retrieveInlineStream_withCR_asEOL() throws Exception {
        this.config.lineEnding = EolType.CR;
        testInlineStream(
                "A\nB\r\nC\rD",
                "A\rB\rC\rD");
    }

    @Test
    public void retrieveInlineStream_withVariableReplacement() throws Exception {
        this.variableManager.valueMap.put("variable", "swapping");
        this.config.variableType = VARIABLE_PATTERN_CHOICES.get(1);
        testInlineStream(
                "Testing ${variable} ${swap:-variable}.",
                "Testing swapping variable.");
    }

    @Test
    public void retrieveInlineStream_withVariableReplacement_forA() throws Exception {
        this.variableManager.valueMap.put("A", "OK");
        this.config.variableType = VARIABLE_PATTERN_CHOICES.get(1);
        testInlineStream(
                "${A}\n$(B)\r\n@(C)\r#[D]\n{{E}}\n[(F)]",
                "OK\n$(B)\n@(C)\n#[D]\n{{E}}\n[(F)]");
    }

    @Test
    public void retrieveInlineStream_withDefaultVariableReplacement_forA() throws Exception {
        this.config.variableType = VARIABLE_PATTERN_CHOICES.get(1);
        testInlineStream(
                "${A:-OK}\n$(B)\r\n@(C)\r#[D]\n{{E}}\n[(F)]",
                "OK\n$(B)\n@(C)\n#[D]\n{{E}}\n[(F)]");
    }

    @Test
    public void retrieveInlineStream_withVariableReplacement_forB() throws Exception {
        this.variableManager.valueMap.put("B", "OK");
        this.config.variableType = VARIABLE_PATTERN_CHOICES.get(2);
        testInlineStream(
                "${A}\n$(B)\r\n@(C)\r#[D]\n{{E}}\n[(F)]",
                "${A}\nOK\n@(C)\n#[D]\n{{E}}\n[(F)]");
    }

    @Test
    public void retrieveInlineStream_withDefaultVariableReplacement_forB() throws Exception {
        this.config.variableType = VARIABLE_PATTERN_CHOICES.get(2);
        testInlineStream(
                "${A}\n$(B:OK)\r\n@(C)\r#[D]\n{{E}}\n[(F)]",
                "${A}\nOK\n@(C)\n#[D]\n{{E}}\n[(F)]");
    }

    @Test
    public void retrieveInlineStream_withVariableReplacement_forC() throws Exception {
        this.variableManager.valueMap.put("C", "OK");
        this.config.variableType = VARIABLE_PATTERN_CHOICES.get(3);
        testInlineStream(
                "${A}\n$(B)\r\n@(C)\r#[D]\n{{E}}\n[(F)]",
                "${A}\n$(B)\nOK\n#[D]\n{{E}}\n[(F)]");
    }

    @Test
    public void retrieveInlineStream_withDefaultVariableReplacement_forC() throws Exception {
        this.config.variableType = VARIABLE_PATTERN_CHOICES.get(3);
        testInlineStream(
                "${A}\n$(B)\r\n@(C/OK)\r#[D]\n{{E}}\n[(F)]",
                "${A}\n$(B)\nOK\n#[D]\n{{E}}\n[(F)]");
    }

    @Test
    public void retrieveInlineStream_withVariableReplacement_forD() throws Exception {
        this.variableManager.valueMap.put("D", "OK");
        this.config.variableType = VARIABLE_PATTERN_CHOICES.get(4);
        testInlineStream(
                "${A}\n$(B)\r\n@(C)\r#[D]\n{{E}}\n[(F)]",
                "${A}\n$(B)\n@(C)\nOK\n{{E}}\n[(F)]");
    }

    @Test
    public void retrieveInlineStream_withDefaultVariableReplacement_forD() throws Exception {
        this.config.variableType = VARIABLE_PATTERN_CHOICES.get(4);
        testInlineStream(
                "${A}\n$(B)\r\n@(C)\r#[D|OK]\n{{E}}\n[(F)]",
                "${A}\n$(B)\n@(C)\nOK\n{{E}}\n[(F)]");
    }

    @Test
    public void retrieveInlineStream_withVariableReplacement_forE() throws Exception {
        this.variableManager.valueMap.put("E", "OK");
        this.config.variableType = VARIABLE_PATTERN_CHOICES.get(5);
        testInlineStream(
                "${A}\n$(B)\r\n@(C)\r#[D]\n{{E}}\n[(F)]",
                "${A}\n$(B)\n@(C)\n#[D]\nOK\n[(F)]");
    }

    @Test
    public void retrieveInlineStream_withDefaultVariableReplacement_forE() throws Exception {
        this.config.variableType = VARIABLE_PATTERN_CHOICES.get(5);
        testInlineStream(
                "${A}\n$(B)\r\n@(C)\r#[D]\n{{E||\"OK\"}}\n[(F)]",
                "${A}\n$(B)\n@(C)\n#[D]\nOK\n[(F)]");
    }

    @Test
    public void retrieveInlineStream_withVariableReplacement_forF() throws Exception {
        this.variableManager.valueMap.put("F", "OK");
        this.config.variableType = VARIABLE_PATTERN_CHOICES.get(6);
        testInlineStream(
                "${A}\n$(B)\r\n@(C)\r#[D]\n{{E}}\n[(F)]",
                "${A}\n$(B)\n@(C)\n#[D]\n{{E}}\nOK");
    }

    @Test
    public void retrieveInlineStream_withDefaultVariableReplacement_forF() throws Exception {
        this.config.variableType = VARIABLE_PATTERN_CHOICES.get(6);
        testInlineStream(
                "${A}\n$(B)\r\n@(C)\r#[D]\n{{E}}\n[(F|OK)]",
                "${A}\n$(B)\n@(C)\n#[D]\n{{E}}\nOK");
    }

    private void testInlineStream(String input, String output) throws IOException {
        this.config.inlineBody = input;

        InputStream inputStream = supplier.retrieveInlineStream();

        String result = IOUtils.toString(inputStream, "UTF-8");
        assertThat(result, Matchers.equalTo(output));
    }

    @Test
    public void retrieveOnlineStream_withVariableReplacement() throws Exception {
        this.config.onlineLocation = getClass().getClassLoader().getResource("retrieveOnlineStreamSampleText.txt");
        this.config.variableType = VARIABLE_PATTERN_CHOICES.get(1);
        this.variableManager.valueMap.put("variable", "swapping");
        this.variableManager.valueMap.put("swap", "variable");

        InputStream inputStream = supplier.retrieveOnlineStream();

        String result = IOUtils.toString(inputStream, "UTF-8");
        assertThat(result, Matchers.equalTo("Testing swapping variable."));
    }
}