describe('Filedownloader', () => {
  beforeEach(() => {
    cy.visit('/')
    cy.contains('Log in').click()
    cy.get('input[name=os_username]').type('admin')
    cy.get('input[name=os_password]').type('admin')
    cy.get('input[type=submit]').contains('Log in').click({ force: true })
  })

  it('Test plan', () => {
    cy.visit('/admin/agent/configureSharedLocalCapabilities.action')
    cy.get('#updateDefaultsCapabilities').click()
    cy.get('#addCapability_capabilityType').select('Executable')
    cy.get('#executableTypeSelect').select('Command')
    cy.get('#addCapability_builderLabel').type('Python 3')
    cy.get('#addCapability_builderPath').type('/usr/bin/python3')
    cy.get('#addCapability_save').click({ force: true })
    cy.log('Updated capabilities.')

    cy.visit('/admin/configureLinkedRepositories!doDefault.action')
    cy.get('#addRepository').click()
    cy.contains('Bitbucket Cloud').click()
    cy.get('#repositoryName').type('File Downloader')
    cy.get('#createLinkedRepository_repository_bitbucket_accessLevel').select('Public')
    cy.contains('label', 'Owner').siblings('input').type('armotic')
    cy.get('#repository-bitbucket-load-repositories').click()
    cy.get('#fieldArea_createLinkedRepository_repository_bitbucket_repository').should('have.class', 'loading')
    cy.get('#fieldArea_createLinkedRepository_repository_bitbucket_repository').should('not.have.class', 'loading')
    cy.get('#s2id_createLinkedRepository_repository_bitbucket_repository > .select2-choice > .select2-chosen').click()
    cy.get('.select2-result-label').contains('armotic/file-downloader').click()
    cy.contains('Test connection').click()
    cy.get('.connection-result-message > p').contains('Connection successful')
    cy.get('#createLinkedRepository_save').click({ force: true })
    cy.log('Created Linked Repository.')

    cy.get('button#createPlanLink').contains('Create').click()
    cy.get('a#createNewProject').contains('Create project').click()
    cy.get('input[name=projectName]').type('file-downloader')
    cy.get('input[name=projectKey]').clear().type('FD')
    cy.get('input[type=submit]').contains('Save').click({ force: true })
    cy.log('Project was created.')

    cy.get('button#createPlanLink').contains('Create').click()
    cy.get('a#createNewRssPlan').contains('Create Bamboo Specs').click()
    cy.get('input#buildProjectOption').check()
    cy.get('#s2id_selectProjectKey > .select2-choice > .select2-chosen').click()
    cy.contains('file-downloader').click()
    cy.get('input#repositoryTypeCreateOption').check()
    cy.get('a#repository-other').click()
    cy.get('a').contains('Bitbucket Cloud').click()
    cy.get('input[name=repositoryName]').type('File Downloader Specs')
    cy.get('select#createSpecs_repository_bitbucket_accessLevel').select('Public')
    cy.contains('label', 'Owner').siblings('input').type('armotic')
    cy.get('#repository-bitbucket-load-repositories').click()
    cy.get('#fieldArea_createSpecs_repository_bitbucket_repository').should('have.class', 'loading')
    cy.get('#fieldArea_createSpecs_repository_bitbucket_repository').should('not.have.class', 'loading')
    cy.get('#s2id_createSpecs_repository_bitbucket_repository > .select2-choice > .select2-chosen').click()
    cy.get('.select2-result-label').contains('armotic/file-downloader-specs').click()
    cy.contains('Test connection').click()
    cy.get('.connection-result-message > p').contains('Connection successful')
    cy.get('form#createSpecs').submit()
    cy.log('Specs were updated.')

    cy.get('#healthcheck-notification > .aui-message > .aui-icon').click()
    cy.wait(10000)
    cy.pause()

    cy.visit('/build/admin/edit/editBuildTasks.action?buildKey=FD-FIP-TIP')
    cy.get('a > .item-description').contains('Download README.md from repository').click()
    cy.get('form#updateTask').submit()

    cy.visit('/browse/FD-FIP')
    cy.get('button').contains('Run').click()
    cy.get('a').contains('Run plan').click()
    cy.get('#sr-build').should('have.class', 'Queued')
    cy.get('#sr-build', { timeout: 60000 }).should('have.class', 'InProgress')
    cy.get('#sr-build', { timeout: 300000 }).should('have.class', 'Successful')
  })
})
