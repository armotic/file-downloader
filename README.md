File Downloader 1.4.1
=====================
    Contributors: 'brice.ruppen@armotic.fr'
    Requires at least: 6.0.0
    Tested up to: 6.7.1
    License: Apache License 2.0
    License URI: https://bitbucket.org/armotic/file-downloader/src/master/LICENSE
    Wiki: https://bitbucket.org/armotic/file-downloader/wiki/Home

This is a Bamboo plugin that helps you download a file or even unpack an archive into your workspace.
