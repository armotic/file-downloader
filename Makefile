.EXPORT_ALL_VARIABLES:
PATH := ${PWD}/sdk/bin:${PATH}
MVN  ?= atlas-mvn
AMPS_PLUGIN_VERSION := $(shell cat pom.xml | grep '<amps.version>' | sed -rn 's/.*>([[:digit:].]+)<.*/\1/p')

help::
	@grep '^[^#[:space:]]*::\?.*#.*' Makefile | GREP_COLOR='01;34' grep --color=always '^[^:]*' | GREP_COLOR='01;36' grep --color '[^#]*'


build:: # Build the plugin
	${MVN} clean install

docker-build:: # Build with docker image
	docker build --build-arg AMPS_PLUGIN_VERSION=${AMPS_PLUGIN_VERSION} -t fd-sdk .
	mkdir -p ${PWD}/.m2


run:: # Run a Bamboo server with file-downloader installed
	${MVN} com.atlassian.maven.plugins:maven-amps-dispatcher-plugin:${AMPS_PLUGIN_VERSION}:run -Datlassian.plugins.enable.wait=300

docker-run:: docker-build # Run a Bamboo server inside a container with file-downloader installed
	docker run -it --name fd-build --rm -p 6990:6990 \
	  -v ${PWD}/.m2:/root/.m2 \
	  fd-sdk \
	  atlas-run -Datlassian.plugins.enable.wait=300


debug:: # Debug file-downloader through a local Bamboo server
	${MVN} com.atlassian.maven.plugins:maven-amps-dispatcher-plugin:${AMPS_PLUGIN_VERSION}:debug -Datlassian.plugins.enable.wait=300

docker-debug:: docker-build # Debug file-downloader through a Docker container
	docker run -it --name fd-build --rm -p 6990:6990 -p 5005:5005 \
	  -v ${PWD}/.m2:/root/.m2 \
	  fd-sdk \
	  atlas-debug -Datlassian.plugins.enable.wait=300


test:: # Run automated test
	npm test


sdk: # Install Atlassian' sdk if not present
	curl -Lo atlassian-sdk.tg https://marketplace.atlassian.com/download/plugins/atlassian-plugin-sdk-tgz
	@mkdir -p sdk
	tar -xf atlassian-sdk.tgz -C sdk --strip 1


version:: # Print versions
	@cat README.md | head -n 1
	@atlas-version 